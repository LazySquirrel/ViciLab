# ![ViciLab](ViciLabLogo.png)

Blogs, Media, Data Sharing, Password Management, even Social Networks.. It's easy to deploy. Conquer it all, with ViciLab.

#### [Documentation](https://vicilab.net/docs/)

#### [Installation](https://vicilab.net/docs/setup/installation/)

## Summary

Deploys web services to a server. Deployment is managed by Ansible, the services are managed by Docker.

## Goals

To make it easy for anyone to run their own services and own all their data in an easy and secure way, without the need of service or cloud providers.

## Features

- [Manual deployment](https://vicilab.net/docs/setup/installation/#manual-set-up)
- Automated Backups
- Easy Restore
- Automated Tor Onion Service access
- Automated HTTPS via LetsEncrypt
- [Automated Settings Sync](https://vicilab.net/docs/setup/installation/#syncing-settings-via-git)
- Optional Cloud Bastion Server with WireGuard VPN

### [Planned Features](https://codeberg.com/Resonance/ViciLab/labels/enhancement)

### [Available Software](https://vicilab.net/docs/#available-software)

## Get Support

- [File an issue on Codeberg](https://codeberg.com/Resonance/ViciLab/issues).
- [Talk to us on ViciLab Zulip Chat](https://vicilab.zulipchat.com/)
- [Follow ViciLab on Mastodon](https://fosstodon.org/@vicilab)
- [Ask a question on ViciLab Reddit](https://www.reddit.com/r/ViciLab/)


## Give Support

Help to fix or extend the code, is always welcome and will never be discouraged.
Guidelines are available [here](https://vicilab.net/docs/development/contributing/); be sure to create an issue on codeberg or join us on zulip to discuss your proposed changes

Regular contributors, will be able to join the team (at the discretion of the ViciLab Core Team).
