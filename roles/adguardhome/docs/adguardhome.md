# AdGuard Home

[AdGuard Home](https://adguard.com/en/adguard-home/overview.html) Network-wide software for blocking ads and tracking.

**Supported Architectures:** amd64, 386, armv6, armv7, arm64, ppc64le

## Setup

To enable AdGuard Home, run the following command:

**`vlab set adguardhome.enable true`**

alternatively, you can set the appropriate service settings in `settings/config.yml` to true

eg.
```
adguardhome:
  enable: True
```

To finalise any changes made, please run:

**`vlab update_one service=adguardhome`**

## First Run

!!! DEVELOPERS: make sure that you include any information that the user requires to get started here. !!!

!!! Below are some example headings (3 hashes), with some example instructions!!!

### ADMINISTRATOR SETUP

Navigate to *https://your.service.com/admin*

Create an account with your desired username, as this is the first user <SERVICE> makes this account admin.

### SMTP/ MAIL

1. run **`vlab decrypt`** to decrypt the `vault.yml` file

2. make some changes, eg.

```
# SMTP Settings
smtp:
  host: <the host url>
  port: <typically 587>
  user: <username>
  pass: <password>
  from_email: <host should provide this>
  from_name: <you can decide this>
```
3. Then run **`vlab update_one service=adguardhome`** to complete the changes


## ACCESS

AdGuard Home is available at [https://{% if adguardhome.domain %}{{ adguardhome.domain }}{% else %}{{ service_domain }}{% endif %}/

{% if enable_tor %}
It is also available via Tor at [http://{{ adguardhome.subdomain + "." + tor_domain }}/](http://{{ adguardhome.subdomain + "." + tor_domain }}/)
{% endif %}

## SECURITY/ HTTPS_ONLY/ AUTH

To enable https_only or auth, run the appropriate command:

**`vlab set adguardhome.https_only true`**  or

**`vlab set adguardhome.auth true`**

then run **`vlab update_one service=adguardhome`** to finalise the changes

alternatively, you can set the appropriate service settings in `settings/config.yml` to true

eg. (https_only)
```
adguardhome:
  https_only: True
  auth: 
```


More Information can be found in the [documentation](https://vicilab.net), additional assistance can be found in our [Zulip Chat](https://vicilab.zulipchat.org).