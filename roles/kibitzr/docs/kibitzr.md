# Kibitzr

[Kibitzr](https://kibitzr.github.io/) acts as a replacement for IFTTT.

## Setup

Create your `kibitzr.yml` and `kibitzr-creds.yml` in `roles/vicilab/files/`.
You can find examples on the [Kibitzr website](https://kibitzr.github.io/).
