# PackageTitleCase

[PackageTitleCase](PackageURL) PackageOneLiner

**Supported Architectures:** amd64  !!! DEVELOPERS: please do your research, and populate this properly !!!

## Setup

To enable PackageTitleCase, run the following command:

**`vlab set PackageFileName.enable true`**

alternatively, you can set the appropriate service settings in `settings/config.yml` to true

eg.
```
PackageFileName:
  enable: True
```

To finalise any changes made, please run:

**`vlab update_one=PackageFileName`**

## First Run

!!! DEVELOPERS: make sure that you include any information that the user requires to get started here. !!!

!!! Below are some example headings (3 hashes), with some example instructions!!!

### ADMINISTRATOR SETUP

Navigate to *https://your.service.com/admin*

Create an account with your desired username, as this is the first user <SERVICE> makes this account admin.

### SMTP/ MAIL

1. run **`vlab decrypt`** to decrypt the `vault.yml` file

2. make some changes

```
# SMTP Settings
smtp:
  host: <the host url>
  port: <typically 587>
  user: <username>
  pass: <password>
  from_email: <host should provide this>
  from_name: <you can decide this>
```
3. Then run **`vlab update_one <SERVICE>`** to complete the changes


## ACCESS

PackageTitleCase is available at [https://{% if PackageFileName.domain %}{{ PackageFileName.domain }}{% else %}{{ PackageFileName.subdomain + "." + domain }}{% endif %}/](https://{% if PackageFileName.domain %}{{ PackageFileName.domain }}{% else %}{{ PackageFileName.subdomain + "." + domain }}{% endif %}/) or [http://{% if PackageFileName.domain %}{{ PackageFileName.domain }}{% else %}{{ PackageFileName.subdomain + "." + domain }}{% endif %}/](http://{% if PackageFileName.domain %}{{ PackageFileName.domain }}{% else %}{{ PackageFileName.subdomain + "." + domain }}{% endif %}/)

{% if enable_tor %}
It is also available via Tor at [http://{{ PackageFileName.subdomain + "." + tor_domain }}/](http://{{ PackageFileName.subdomain + "." + tor_domain }}/)
{% endif %}

## SECURITY/ HTTPS_ONLY/ AUTH

To enable https_only or auth, run the appropriate command:

**`vlab set PackageFileName.https_only true`**  or

**`vlab set PackageFileName.auth true`**

then run **`vlab update_one PackageFileName`** to finalise the changes

alternatively, you can set the appropriate service settings in `settings/config.yml` to true, eg.
```
PackageFileName:
  https_only: true
  auth: true
```
 and then run **`vlab update_one PackageFileName`** to finalize your changes.


More Information can be found in the [documentation](https://vicilab.net), additional assistance can be found in our [Zulip Chat](https://vicilab.zulipchat.org).
