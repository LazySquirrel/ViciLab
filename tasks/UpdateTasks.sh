#!/usr/bin/env bash

# Updates all services on the ViciLab server
Task::update() {
  : @desc "Updates all services on the ViciLab Server"
  : @param config_dir="settings"
  : @param debug true "Debugs ansible-playbook commands"
  #Task::logo
  #Task::build
  #Task::git_sync
  #Task::config

  highlight "Updating ViciLab Services using $_config_dir"
  Task::run_docker ansible-playbook $(debug_check) --extra-vars="@$_config_dir/config.yml" --extra-vars="@$_config_dir/vault.yml" -i inventory -t deploy playbook.vicilab.yml
  Task::run_docker ansible-playbook $(debug_check) --extra-vars="@$_config_dir/config.yml" --extra-vars="@$_config_dir/vault.yml" -i inventory playbook.restart.yml
  highlight "Update Complete"
}

# Updates the specified service on the ViciLab server
Task::update_one(){
  : @desc "Updates the specified service on the ViciLab server"
  : @param service "Service Name"
  : @param config_dir="settings"
  : @param force true "Forces a rebuild/repull of the docker image"
  : @param build true "Builds the image locally"
  : @param debug true "Debugs ansible-playbook commands"

  Task::logo
  Task::build $(build_check) $(force_check)

  Task::git_sync
  Task::config

  Task::run_docker ansible-playbook $(debug_check) --extra-vars='{"services":["'${_service}'"]}' --extra-vars="@$_config_dir/config.yml" --extra-vars="@$_config_dir/vault.yml" -i inventory -t deploy playbook.vicilab.yml
  Task::run_docker ansible-playbook $(debug_check) --extra-vars='{"services":["'${_service}'"]}' --extra-vars="@$_config_dir/config.yml" --extra-vars="@$_config_dir/vault.yml" -i inventory playbook.restart.yml
}
