#!/usr/bin/env bash

# Contains VLAB specific tasks related to on-this-host tasks.

# Prints the Logo
Task::logo() {
    : @desc "Prints the Logo"

  if [[ -v "already_ran[${FUNCNAME[0]}]" ]] ;  then return ; fi
  already_ran[${FUNCNAME[0]}]=1
  cat vicilabascii.txt
  Task::check_version
  printf "MOTD:\n\n" && cat MOTD || printf "Could not get MOTD"
  printf "\n\n"

  Task::sanity_check
}

Task::generate_ansible_pass(){
  if [[ "$OSTYPE" == "darwin"* ]]; then
    function sha256sum() { shasum -a 256 "$@" ; } && export -f sha256sum
  fi
  date +%s | sha256sum | base64 | head -c 32  > ~/.vlab_vault_pass
}

#Shows the /docs/software/service.md rendered in the terminal
Task::show(){
  : @desc "Show the docs for the specified service"
  : @param service "Service name: service=serviceName"

  Task::run_docker mdv -t 729.8953 docs/software/${_service}.md
  highlight "Current Configuration settings"
  Task::show_config
}

# Builds the docker image used for ViciLab Deployments
Task::build() {
    : @desc "Builds the Docker Image used to deploy"
    : @param force true "Forces a rebuild/repull of the docker image"
    : @param build true "Builds the image locally"

  if [[ -v "already_ran[${FUNCNAME[0]}]" ]] ;  then return ; fi

  if [[ ${_force-true} == true ]] ; then
    vicilab_docker=$(docker images -a | grep "vicilab" | awk '{print $3}')
    if [[ -n ${vicilab_docker} ]]; then
      sudo docker rmi --force ${vicilab_docker}
    fi
    sudo docker build -t vicilab:$VERSION .
  fi

  if [[ ${_build-true} == true ]] ; then
    sudo docker build -t vicilab:$VERSION .
  fi

  if [[ -v "already_ran[${FUNCNAME[0]}]" ]] ; then exit 0; fi
  already_ran[${FUNCNAME[0]}]=1
  highlight "Preparing ViciLab Docker Image"
  sudo docker inspect --type=image vicilab:$VERSION > /dev/null && highlight " Docker Image Already Built" || sudo docker build -t vicilab:$VERSION .
  already_ran[${FUNCNAME[0]}]=1

}

# Manually forces a settings Sync via Git
Task::git_sync() {
  : @desc "Manually forces a settings sync via git"

  local return_dir=$PWD
  if [[ -v "already_ran[${FUNCNAME[0]}]" ]] ;  then return ; fi
  already_ran[${FUNCNAME[0]}]=1
  mkdir -p settings > /dev/null 2>&1
  # If there is a git repo, then attempt to update
  if [ -d settings/.git/ ]; then
    mkdir -p settings/.git/hooks/ > /dev/null 2>&1
    cp git_sync_pre_commit settings/.git/hooks/pre-commit
    chmod +x settings/.git/hooks/pre-commit
    cd settings
    colorize yellow "Syncing settings via Git"
    git pull
    git add * > /dev/null
    git commit -a -m "Settings update" || true
    git push > /dev/null
  else
    colorize yellow " Warning! You do not have a git repo set up for your settings. Make sure to back them up using some other method. https://vicilab.net/docs/setup/installation/#syncing-settings-via-git "
  fi
  cd $return_dir

}

# Encrypt the vault
Task::encrypt(){
  : @desc "Encrypts the vault"
  
  highlight "Encrypting Vault"
    local userID=$(id -u)
    local groupID=$(id -g)
  Task::run_docker ansible-vault encrypt settings/vault.yml
  sudo chmod 640 settings/vault.yml
  sudo chown $userID:$groupID settings/vault.yml
  highlight "Vault encrypted!"
}

# Decrypts the vault
Task::decrypt(){
  : @desc "Decrypts the vault"
  
  highlight "Decrypting Vault"
    local userID=$(id -u)
    local groupID=$(id -g)
  Task::run_docker ansible-vault decrypt settings/vault.yml || true
  sudo chmod 640 settings/vault.yml
  sudo chown $userID:$groupID settings/vault.yml
  highlight "Vault decrypted!"
}

# Uninstalls ViciLab
Task::uninstall(){
  : @desc "Uninstalls ViciLab"
  : @param config_dir="settings"
  : @param force true "Forces a rebuild/repull of the docker image"
  : @param build true "Builds the image locally"
  : @param debug true "Debugs ansible-playbook commands"

  Task::logo
  Task::build $(build_check) $(force_check)

  highlight "Uninstall ViciLab Completely"
  Task::run_docker ansible-playbook $(debug_check) --extra-vars="@$_config_dir/config.yml" --extra-vars="@$_config_dir/vault.yml" -i inventory -t deploy playbook.remove.yml
  highlight "Uninstall Complete"
}

# Restores a server from Backups. Assuming Backups were running
Task::restore() {
  : @desc "Restore a server from backups. Assuming backups were running"
  : @param config_dir="settings"
  : @param debug true "Debugs ansible-playbook commands"

  Task::run_docker ansible-playbook $(debug_check) --extra-vars="@$_config_dir/config.yml" --extra-vars="@$_config_dir/vault.yml" -i inventory playbook.restore.yml
}

# Opens a shell on the ViciLab deploy server
Task::shell() {
  : @desc "Opens a shell in the ViciLab deploy server"
   ssh $(vlab_ssh_user)@$(vlab_ip) -p $(vlab_port)
}

# Opens a shell in the ViciLab deploy container
Task::vicilab_shell() {
  : @desc "Opens a shell in the deployed ViciLab docker container"

  Task::run_docker /bin/bash
}

# Allows you to switch between various branches and tags of VLAB
Task::track(){
  : @desc "Switches you to the specified branch or tag. use branch=<branchname>"
  : @param branch! "Required! Branch or tag name to track"

  git checkout $_branch
}

Task::run_docker() {
  docker run --rm -it \
  -v $HOME/.ssh/$(pwless_sshkey):/$(vlab_ssh_user)/.ssh/$(pwless_sshkey) \
  -v $HOME/.ssh/$(pwless_sshkey).pub:/$(vlab_ssh_user)/.ssh/$(pwless_sshkey).pub \
  -v $(pwd):/data \
  -v $HOME/.vlab_vault_pass:/ansible_vault_pass \
  vicilab:${VERSION} "$@"
}

# Checks the current version
Task::check_version() {
  : @desc "Checks the current version"

  VERSION_CURRENT=$(cat VERSION)
  VERSION_LATEST=$(cat VERSION)
  #VERSION_LATEST=$(curl -s -m 2 https://codeberg.com/Resonance/ViciLab/raw/master/VERSION)

  function version_gt() { test "$(echo "$@" | tr " " "\n" | sort | head -n 1)" != "$1"; }

  colorize yellow "You currently have version: $VERSION_CURRENT"
  colorize green "The newest Version available is: $VERSION_LATEST"

  if version_gt $VERSION_LATEST $VERSION_CURRENT; then
    colorize red "* You should update to version $VERSION_LATEST! *"
    colorize red " * Update at https://codeberg.com/Resonance/ViciLab/-/releases *"
  else
    colorize green "You are up to date!"
  fi
}

# Links the vlab command into /usr/local/bin
Task::install_cli() {
  : @desc "Links the vlab cli into /usr/local/bin so you can call vlab without the ./"

  if [[ ! -L "/usr/local/bin/vlab"  ]]; then
    sudo ln -s $PWD/vlab /usr/local/bin/vlab
    echo "vlab command line installed"
  else
    sudo rm -f /usr/local/bin/vlab
    sudo ln -s $PWD/vlab /usr/local/bin/vlab
    echo "vlab command line re-installed" || true
  fi
}
