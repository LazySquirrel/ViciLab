#!/usr/bin/env bash

# Helper Functions

function highlight() {
  termwidth="$(tput cols)"
  padding="$(printf '%0.1s' ={1..500})"
  colorize yellow $(printf '%*.*s %s %*.*s\n' 0 "$(((termwidth-2-${#1})/16))" "$padding" "$1" 0 "$(((termwidth-1-${#1})/2))" "$padding")
}

function build_check() {
  if [[ ${_build-true} == true ]] ; then
    echo "build=true"
  else
    echo ""
  fi
}

function force_check() {
  if [[ ${_force-true} == true ]] ; then
    echo "force=true"
  else
    echo ""
  fi
}

function debug_check() {
  if [[ ${_debug-true} == true ]] ; then
    echo "-vvvv"
  else
    echo ""
  fi
}

# Ansible -> Bash Variables

function vlab_ip (){
if [[ -f tasks/Vars ]]; then
  source tasks/Vars
  if [[ VLAB_IP == "" ]]; then
    echo "localhost"
  else
    echo "$VLAB_IP"
  fi
  else
  echo "localhost"
fi
}

function vlab_port () {
if [[ -f tasks/Vars ]]; then
  source tasks/Vars
  if [[ VLAB_PORT == "" ]]; then
    echo "22"
  else
    echo "$VLAB_PORT"
  fi
  else
  echo "22"
fi
}

function vlab_ssh_user () {
if [[ -f tasks/Vars ]]; then
  source tasks/Vars
  if [[ VLAB_SSH_USER == "" ]]; then
    echo "root"
  else
    echo "$VLAB_SSH_USER"
  fi
  else
  echo "root"
fi
}

function admin_email () {
if [[ -f tasks/Vars ]]; then
  source tasks/Vars
  if [[ ADMIN_EMAIL == "" ]]; then
    echo "admin_email@vicilab.net"
  else
    echo "$ADMIN_EMAIL"
  fi
  else
  echo "admin_email@vicilab.net"
fi
}

function default_username () {
if [[ -f tasks/Vars ]]; then
  source tasks/Vars
  if [[ DEFAULT_USERNAME == "" ]]; then
    echo "admin"
  else
    echo "$DEFAULT_USERNAME"
  fi
  else
  echo "admin"
fi
}

function arm-check () {
if [[ -f tasks/Vars ]]; then
  source tasks/Vars
  if [[ ARM == "" ]]; then
    echo "False"
  else
    echo "$ARM"
  fi
  else
  echo "False"
fi
}

function pwless_sshkey () {
if [[ -f tasks/Vars ]]; then
  source tasks/Vars
  if [[ PASSWORDLESS_SSHKEY == "" ]]; then
    echo "id_rsa"
  else
    echo "$PASSWORDLESS_SSHKEY"
  fi
  else
  echo "id_rsa"
fi
}
