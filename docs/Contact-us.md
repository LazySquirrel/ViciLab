# Contacting ViciLab

Thank you for attempting to reach us. Our small development team is passionate about this project, and we hope that you will be too.

## Code Issues/ Suggestions/ Requests
We look forward to hearing from you; and to those of you that want to contribute to ViciLab, we are always happy to review and discuss any contributions.

In regards to contributions, code or otherwise, the [ViciLab Codeberg page](https://codeberg.com/ViciLab/ViciLab) is the best place to open an issue (or a suggestion). The [ViciLab Zulip Chat](https://vicilab.zulipchat.com/) is also available for discussion; however opening an issue on Codeberg will help the project stay on track.

## Community Issues/ Complaints/ Feedback
ViciLab prefers that any issues involving the community be referred to one of the Community Leaders via the [ViciLab Zulip Chat](https://vicilab.zulipchat.com/). Feel free to ask for further information in the 'Community Support' thread.

## Disclaimer/ Notes

We suggest that you consult the ViciLab [Code of Conduct](https://codeberg.com/ViciLab/ViciLab/wiki/Code-of-Conduct) for more information on how your enquiry, and information are handled.

Please understand that everyone here is busy, and that this is an open source project. That means that all the code is available for you to audit; however, it also means that the team is volunteering their time to this project, and understandably there may be some short delays in getting back to you.
