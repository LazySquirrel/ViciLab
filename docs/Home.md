# ViciLab

### Welcome to ViciLab!
Blogs, Media, Data Sharing, Password Management, even Social Networks.. It's easy to deploy. Conquer it all, with ViciLab.

ViciLab allows you to deploy a collection of various separate services, from any client computer to your own remote server.
You can find more information about ViciLab in the menu on the left. Search is also available to you.

#### Installation
a.k.a The first place you should visit. Installation instructions live [here](core/Installation.md)

#### Next Steps
What to do next? It wouldn't hurt to learn a bit more. Check [this](core/Next-Step.md) out

#### Commands
ViciLab has a few commands to make deploying your server, easy. You can find them [here](core/Commands.md)

#### Understanding Storage
Storage is a big deal with any server. Check out how it's handled with ViciLab [here](core/Storage.md)

## Getting Help

If you are having problems you can:

- [File an issue on Codeberg](https://codeberg.com/Resonance/ViciLab/issues).
- [Talk to us on ViciLab Zulip Chat](https://vicilab.zulipchat.com/)
- [Follow ViciLab on Mastodon](https://fosstodon.org/@vicilab)
- [Ask a question on ViciLab Reddit](https://www.reddit.com/r/ViciLab/)

## Available Software

### Categories

- [Analytics](software/INDEX.md#analytics)
- [Automation](software/INDEX.md#automation)
- [Blogging Platforms](software/INDEX.md#blogging-platforms)
- [Calendaring and Contacts Management](software/INDEX.md#calendaring-and-contacts-management)
- [Chat](software/INDEX.md#chat)
- [Document Management](software/INDEX.md#document-management)
- [E-books](software/INDEX.md#e-books)
- [Email](software/INDEX.md#email)
- [Federated Identity/Authentication](software/INDEX.md#federated-identityauthentication)
- [Feed Readers](software/INDEX.md#feed-readers)
- [File Sharing and Synchronization](software/INDEX.md#file-sharing-and-synchronization)
- [Gateways and terminal sharing](software/INDEX.md#gateways-and-terminal-sharing)
- [Media Streaming](software/INDEX.md#media-streaming)
- [Misc/Other](software/INDEX.md#miscother)
- [Money, Budgeting and Management](software/INDEX.md#money-budgeting-and-management)
- [Monitoring](software/INDEX.md#monitoring)
- [Note-taking and Editors](software/INDEX.md#note-taking-and-editors)
- [Password Managers](software/INDEX.md#password-managers)
- [Personal Dashboards](software/INDEX.md#personal-dashboards)
- [Photo and Video Galleries](software/INDEX.md#photo-and-video-galleries)
- [Read it Later Lists](software/INDEX.md#read-it-later-lists)
- [Software Development](software/INDEX.md#software-development)
- [Task management/To-do lists](software/INDEX.md#task-managementto-do-lists)
- [VPN](software/INDEX.md#vpn)
- [Web servers](software/INDEX.md#web-applications)
- [Wikis](software/INDEX.md#wikis)
