# Available Services:

## Analytics:
* [Matomo](matomo.md) - 

## Automation:
* [Home Assistant](homeassistant.md) - 
* [HomeBridge](homebridge.md) - 
* [Kibitzr](kibitzr.md) - 

## Blogging Platforms:
* [Ghost](ghost.md) - 

## Calendaring and Contacts Management:
* [NextCloud](nextcloud.md) - 
* [Monica](monica.md) - 

## Chat:
* [Matterbridge](matterbridge.md) - 
* [The Lounge](thelounge.md) - 
* [Zulip](zulip.md) - 

## Document Management:
* [Mayan EDMS](mayan.md) - 
* [Paperless](paperless.md) - 
* [Teedy](teedy.md) - 

## E-Books:
* [Calibre](calibre.md) - 
* [LazyLibrarian](lazylibrarian.md) - 

## Email:
* [Mailu](mailu.md) - 

## Federated Identity/Authentication:
* [Authelia](authelia.md) - 
* [OpenLDAP](openldap.md) - 
* [Keycloak](keycloak.md) - 

## Feed Readers:
* [FreshRSS](freshrss.md) - 
* [Miniflux](miniflux.md) - 
* [RSSHub](rsshub.md) -  

## File Sharing and Synchronization:
* [Duplicati](duplicati.md) - 
* [Jackett](jackett.md) - 
* [Lidarr](lidarr.md) - 
* [Minio](minio.md) - 
* [Mylar](mylar.md) - 
* [Ombi](ombi.md) - 
* [qBittorrent](qbittorrent.md) - 
* [Radarr](radarr-sonarr.md) - 
* [Samba](samba.md) - 
* [SickChill](sickchill.md) - 
* [Sonarr](radarr-sonarr.md) - 
* [Syncthing](syncthing.md) - 
* [Transmission](transmission.md) - 
* [WebDAV](webdavserver.md) - 

## Games:
* [Factorio](factorio.md) - 
* [Minecraft](minecraft.md) - 
* [MinecraftBedrockServer](minecraftbedrockserver.md) - 
* [MassiveDecks](massivedecks.md) - 

## Gateways and terminal sharing:
* [Guacamole](guacamole.md) - 
* [WebVirtMgr](webvirtmgr.md) - 

## Media Streaming:
* [Airsonic](airsonic.md) - 
* [Beets](beets.md) - 
* [Emby](emby.md) - 
* [Funkwhale](funkwhale.md) - 
* [Jellyfin](jellyfin.md) - 
* [MStream](mstream.md) - 
* [PeerTube](peertube.md) - 
* [Plex](plex.md) - 

## Misc/Other:
* [AdGuard Home](adguardhome.md) - 
* [Barcode Buddy](barcodebuddy.md) - 
* [Chowdown](chowdown.md) - 
* [Cockpit](system-cockpit.md) - 
* [DuckDNS](duckdns.md) - 
* [EtherCalc](ethercalc.md) - 
* [ERPNext](erpnext.md) - 
* [Folding@home](folding_at_home.md) - 
* [Grocy](grocy.md) - 
* [Gotify](gotify.md) - 
* [Inventario](inventario.md) - 
* [Mashio](mashio.md) - 
* [n8n](n8n.md) - 
* [NodeRED](nodered.md) - 
* [OctoPrint](octoprint.md) - 
* [Pi-hole](pihole.md) - 
* [Poli](poli.md) - 
* [PrivateBin](privatebin.md) - 
* [Sabnzbd](sabnzbd.md) - 
* [Searx](searx.md) - 
* [Shinobi](shinobi.md) - 
* [Ubooquity](ubooquity.md) - 
* [Watchtower](watchtower.md) - 
* [WebTrees](webtrees.md) - 
* [Zammad](zammad.md) - 

## Money, Budgeting and Management:
* [Firefly III](firefly.md) - 
* [InvoiceNinja](invoiceninja.md) - 

## Monitoring
* [ELK Stack](elkstack.md) - 
* [Grafana](grafana.md) - 
* [HealthChecks](healthchecks.md) - 
* [Portainer](portainer.md) - 
* [Taisun](taisun.md) - 
* [NetData](netdata.md) - 
* [Speedtest](speedtest.md) - 
* [Statping](statping.md) - 
* [Tautulli](tautulli.md) - 
* [Telegraf, InfluxDB, Chronograf, Kapacitor](tick.md) - 

## Note-taking and Editors:
* [BulletNotes](bulletnotes.md) - 
* [Trilium](trilium.md) - 
* [Turtl](turtl.md) - 

## Password Managers:
* [Bitwarden](bitwarden.md) - 

## Personal Dashboards:
* [Homedash](homedash.md) - 
* [Heimdall](heimdall.md) - 
* [Organizr](organizr.md) - 
* [SUI](sui.md) - 

## Photo and Video Galleries:
* [Digikam](digikam.md) - 
* [Piwigo](piwigo.md) - 
* [Pixelfed](pixelfed.md) - 
* [OwnPhotos](ownphotos.md) - 
* [PhotoPrism](photoprism.md) - 

## Read it Later Lists:
* [Wallabag](wallabag.md) - 

## Social networks:
* [Pleroma](pleroma.md) - 
* [Hubzilla](hubzilla.md) - 

## Software Development:
* [CodiMD](codimd.md) - 
* [Code-Server](codeserver.md) - 
* [Drone](drone.md) - 
* [Gitea](gitea.md) - 
* [Gitlab](gitlab.md) - 
* [Jenkins](jenkins.md) - 
* [Snibox](snibox.md) - 

## Task management/To-do lists:
* [Wekan](wekan.md) - 

## VPN:
* [OpenVPN](openvpn.md) - 

## Web Applications:
* [Apache](apache2.md) - 
* [Huginn](huginn.md) - 
* [Simply Shorten](simply-shorten.md) - 

## Wikis:
* [BookStack](bookstack.md) - 
* [TiddlyWiki](tiddlywiki.md) - 
